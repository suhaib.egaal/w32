import React from "react";
import "./Tori.css";

const Tori = () => {
  return (
    <div className="tori">
      <div className="first-row">
        <input className="input-text" type="text" placeholder="Hakusana ja/ tai postinumero" />
        <select className="input-select-1" name="what">
          <option id="1">Kaikki osastot</option>
        </select>
        <select className="input-select-2" name="where">
          <option id="1">Koko Suomi</option>
        </select>
      </div>
      <div className="second-row">
        <div className="checkbox-container">
          <label className="check">
            <input className="input-checkbox" type="checkbox" name="Myydään" />
            Myydään<span className="checkmark"></span>
          </label>
          <label className="check">
            <input className="input-checkbox" type="checkbox" name="Ostetaan" />
            Ostetaan<span className="checkmark"></span>
          </label>
          <label className="check">
            <input className="input-checkbox" type="checkbox" name="Vuokrataan" />
            Vuokrataan<span className="checkmark"></span>
          </label>
          <label className="check">
            <input className="input-checkbox" type="checkbox" name="Halutaan vuokrata" />
            Halutaan vuokrata<span className="checkmark"></span>
          </label>
          <label className="check">      
            <input className="input-checkbox" type="checkbox" name="Annetaan" />
            Annetaan<span className="checkmark"></span>
          </label> 
        </div>
        <div>
          <a href="#" className="nearbtn">Tallenna haku</a>
          <button className="btn">Hae</button>
        </div>
      </div>
    </div>
  );
};

export default Tori;
